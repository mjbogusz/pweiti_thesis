# pweiti_thesis
Wzorzec (templatka) pracy dyplomowej dla Wydziału Elektroniki i Technik Informacyjnych Politechniki Warszawskiej dla LaTeXa (pdflatex-a dla ścisłości)

## Wymagania
`latex` z dodatkami (zależnie od dystrybucji, najprościej zainstalować wszystko choć sporo wtedy waży), `pdflatex` i `biber`.

## Użycie
* Skopiuj cały ten katalog jako katalog głównegy swojej pracy
* Dostosuj w pliku głównym (`thesis.tex`):
	* treść strony tytułowej
	* treść streszczeń (plus ew przypisy)
	* rodzaj streszczenia (jedna/dwie strony)
* Utwórz kopie pliku rozdziału 1 (`chapter1.tex`) na tyle rozdziałów, ile potrzebujesz, dodaj je w pliku głównym (polecenie `\input{}`)
* Dodaj swoją treść w plikach rozdziałów
* Dodaj swoje odniesienia w pliku bibliografii (__UWAGA:__ zgodnie z [biblatexem](https://en.wikibooks.org/wiki/LaTeX/Bibliography_Management#biblatex), nie bibtexem!)
* ...
* Zdaj pracę, za którą nie dostaniesz ochrzanu w dziekanacie i dostań tytuł!

### Kompilacja
Do kompilacji warto użyć czegoś, co to zautomatyzuje.
Raczej odradzam kompilację ręczną - by prawidłowo przegenerować np ToC trzeba przemielić główny plik do 3 razy, do tego bibliografia, spisy rysunków/tablic, odnośniki...

Polecane rozwiązania:
* edytor tekstu/kodu z obsługą TeX-a, np Sublime Text z dodatkiem LaTeXTools, polecam
* IDE, np TeXStudio
* cokolwiek do automatycznej generacji PDFów, np program `rubber`

## Uwagi i porady
* By automatycznie wstawić twardą spację po słowach składających się z 1 litery użyj search&replace (znajdź i zamień) w trybie wyrażeń regularnych:
	* znajdź: `(\W[azowi]) `
	* zastąp: `\1~`
* W zależności od instytutu minimalnie różnią się wymagania co do sposobu formatowania streszczeń - niektóre wymagają obu (polskiego i angielskiego) na tej samej stronie (np IAiIS), niektóre na dwóch kolejnych (II). Sprawdź, czego wymagają u Ciebie!

## Dlaczego cała strona tytułowa jest zrobiona od nowa?
Ponieważ lista WTF-ów oficjalnego wzorca strony tytułowej jest długa:
* jest .docx-em
* ma tytuł i nagłówek wstawione jako osadzone obrazki
* w/w obrazki są _dość_ niskiej rozdzielczości
* użyte w w/w obrazkach zostały płatne/licencjonowane/niestandardowe czcionki: Adagio Slab Light i Helvetica (zarządzenie rektora 57/2016) (vide [Osadzone PDFy](#osadzone-pdfy))
* odstępy pomiędzy wierszami zrobione wieloma pustymi liniami o różnych wysokościach/odstępach/etc

## Osadzone PDFy
Specjalne czcionki na stronie tytułowej to wymysł kogoś z Wydziału Architektury w Biurze ds. Promocji i Informacji (i poza kiepską dostępnością mają wiele innych problemów).
Ich pliki można uzyskać np z w/w biura bądź z Oficyny Wydawniczej,
ale powodzenia w użyciu OTF w połączeniu z kerningiem (rozstrzeleniem) w \*TeX-ie (PDFLaTeX nie ogarnia OTF, XeLaTeX kerningu etc).

Stąd w tej templatce wstawione są grafiki wektorowe zembedowane w pdf-ach (utworzone w Inkscape'ie).

## TODO
* streszczenie na dwie strony

# Licencja
Może kiedyś coś konkretnego, na razie [Beerware](https://en.wikipedia.org/wiki/Beerware).

## Uznanie autorstwa
Plik `pweiti_thesis/pweiti_authorship.pdf` pochodzi z https://github.com/ArturB/WUT-Thesis
